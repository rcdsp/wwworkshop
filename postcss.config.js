// =======================================================================
// gulp-postcss configuration
// =======================================================================
module.exports = {
  plugins: [
    require('lost'),
    require('rucksack-css')({autoprefixer: false}),
    require('postcss-cssnext')({warnForDuplicates: false})
  ]
}