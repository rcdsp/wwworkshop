// =======================================================================
// gulpfile.js require modules
// =======================================================================
var gulp         = require('gulp'),
    gutil        = require('gulp-util'),
    gulpif       = require('gulp-if'),
    rename       = require('gulp-rename'),
    assign       = require('lodash.assign'),
    del          = require('del'),
    pug          = require('gulp-pug'),
    sass         = require('gulp-sass'),
    postcss      = require('gulp-postcss'),
    cssnano      = require('gulp-cssnano'),
    uglify       = require('gulp-uglify'),
    sourcemaps   = require('gulp-sourcemaps'),
    source       = require('vinyl-source-stream'),
    buffer       = require('vinyl-buffer'),
    browserify   = require('browserify'),
    babelify     = require('babelify'),
    watchify     = require('watchify'),
    browserSync  = require('browser-sync').create();

// =======================================================================
// config environment and gulp js
// =======================================================================

// Modifying gulp prototype to add some methods we'll use.
gulp.Gulp.prototype.__runTask = gulp.Gulp.prototype._runTask;
gulp.Gulp.prototype._runTask = function(task) {
  this.currentTask = task;
  this.__runTask(task);
}
    
// Debug mode (on/off) will change the mode in which gulp tasks
// are executed, mostly minifying code and emitting sourcemaps or
// not depending on if the mode is on or not. It still can be used
// in any custom gulp task created later.
// do not set it manually, run either gulp debug(on) or gulp default(off)
var debug = false;

// Path object with configuration for default paths,
// this are been used in gulp tasks so change them here
// if you need a different folder structure.
// (sjs path stands for "source js" while djs for "dist js").
var path = {
  src  : './src',
  dist : './dist',
  views: './src/views',
  scss : './src/scss',
  sjs  : './src/js',
  html : './dist',
  css  : './dist/css',
  djs  : './dist/js'
}

// =======================================================================
// gulp individual tasks (don't run unless you know what you're doing)
// =======================================================================

// clean task: 
// removes any .html, .css and .js files from dist folder
gulp.task('clean', () => {
    del.sync(`${path.html}/**/*.html`);
    del.sync(`${path.css}/**/*`);
    del.sync(`${path.djs}/**/*`);
});

// html task: 
// compile pug templates into html files to dist folder
gulp.task('html', () => {
  return gulp
    .src(`${path.views}/view/**/*.pug`)
    .pipe(gulpif(debug,
      pug({pretty: true, data: {dist: false }}).on('error', gutil.log),//if debug true
      pug({pretty: false, data: {dist: true }}).on('error', gutil.log)//else
    ))
    .pipe(gulp.dest(path.html))
    .pipe(browserSync.stream());
});

// css task: 
// compiles from scss, stream to postcss and outputs css 
// to dist folder, postcss config is load from ./postcss.config.js 
gulp.task('css', () => {
  return gulp
    .src(`${path.scss}/main.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', err => {gutil.log(err);}))
    .pipe(postcss().on('error', err => {gutil.log(err);}))
    .pipe(gulpif(!debug,
      cssnano({
        convertValues: {length: false},
        discardComments: {removeAll: true}
      })
    ))
    .pipe(gulpif(debug,
      rename({extname: '.css'}), 
      rename({extname: '.min.css'})
    ))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.css))
    .pipe(browserSync.stream());
});

// lib:css task: 
// process external css libraries with postcss 
// and outputs processed files to dist folder
gulp.task('lib:css', () => {
  return gulp
    .src(`${path.scss}/*.css`)
    .pipe(sourcemaps.init())
    .pipe(postcss().on('error', err => {gutil.log(err);}))
    .pipe(gulpif(!debug,
      cssnano({
        convertValues: {length: false},
        discardComments: {removeAll: true}
      })
    ))
    .pipe(gulpif(!debug,
      rename({extname: '.min.css'})
    ))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.css));
});

// js task:
// config browserify and watchify for updates on js entries
// then we bundle and transpile js files and modules into dist folder
var customOpts = {
    entries: [`${path.sjs}/main.js`],
    debug: true,
    transform: [['babelify', {presets: ["es2015"], sourceMaps: true}]]
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

// bundle js function
function bundle() {
  gutil.log(customOpts.entries[0]);
  return b.bundle()
    .on('error', err => {
      gutil.log(err);
      browserSync.notify(err, 3000);
      this.emit('end');
    })
    .pipe(gulpif(debug, 
      source('bundle.js'),//if debug true
      source('bundle.min.js')//else
    ))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(gulpif(!debug, uglify()))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(`${path.djs}`))
    .pipe(browserSync.stream());
};

// js task and watchify
gulp.task('js', bundle);
b.on('update', bundle);
b.on('log', gutil.log);

// lib:js task: copy or process all external js files
// that aren't required across all the website.
gulp.task('lib:js', () => {
  return gulp
    .src(`${path.sjs}/**/!(*main).js`)
    .pipe(sourcemaps.init())
    .pipe(gulpif(!debug, uglify()))
    .pipe(gulpif(!debug, rename({extname: '.min.js'})))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.djs));
});

// watch task:
// watches for changes in pug and scss files
// (js will be watched with watchify)
gulp.task('watch', () => {
  gulp.watch(`${path.views}/**/*.pug`, ['html']);
  gulp.watch(`${path.scss}/**/*.scss`, ['css']);
  gulp.watch(`${path.scss}/*.css`, ['lib:css']);
  gulp.watch(`${path.sjs}/**/!(*main).js`, ['lib:js']);
})

// browser-sync task: 
// Creates a static server and syncs with current
// files on update.
gulp.task('server', ['html', 'css', 'lib:css', 'js', 'watch',], () => {
  browserSync.init({
    server: path.dist,
    online: true
  });
});

// =======================================================================
// gulp development tasks
// =======================================================================

// Start development environment function
function startMode(taskName) {
  return gulp.task(taskName, function() {
      this.currentTask.name === 'debug:on' ? debug = true : debug = false;
      gutil.log(`debug mode set to: ${debug}`);
      gulp.start(['server']);
    });
}

// Both normal and debug modes of development environment
startMode('debug:on');
startMode('debug:off');

// Start development environment with debug mode on
gulp.task('debug', () => {
  gulpif(!debug, gulp.start('clean'));
  gulp.start('debug:on');
});

// Start development environment with debug mode off
gulp.task('default', () => {
  gulpif(debug, gulp.start('clean'));
  gulp.start('debug:off');
});
