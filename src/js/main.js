// =======================================================================
// App initialization 
// =======================================================================
var angular = require('angular');
var Slideout = require('slideout');

// Initialize Slideout (is defined elsewhere)
var slideout = new Slideout({
  'menu': document.getElementById('side-navigation'),
  'panel': document.getElementById('content-side-shifter'),
  'padding': 256,
  'tolerance': 60,
  'side': 'right'
});

// Getting hamburger button
var hamburger = document.getElementById('hamburger-button');

// Using hamburger for toggling sidebar
hamburger.addEventListener('click', function() {
  hamburger.classList.toggle('is-active');
  slideout.toggle();
});

// Angular application ===================================================